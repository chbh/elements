﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Elements
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SolidColorBrush _arcane, _cold, _fire, _holy, _lightning, _physical, _poison;
        private SolidColorBrush[] _wizardColors, _wdColors, _barbColors, _crusaderColors, _monkColors, _dhColors;
        private string[] _elements = {"Cold", "Fire", "Physical", "Poison"};
        private string _class = " - WD";
        private SolidColorBrush[] _currentColors;
        private int _elementSeconds;
        private readonly DispatcherTimer _elementsTimer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            lblElement.Content = "CoE";
            InitColors();
            _elementsTimer.Tick += ElementsTimerTick;
            var hotKey = new HotKey(Key.F1, KeyModifier.Ctrl, OnHotKeyHandler);
        }

        private void OnHotKeyHandler(HotKey hotKey)
        {
            _elementsTimer.Stop();
            StartTimer();
        }

        private void InitColors()
        {
            _arcane = new SolidColorBrush(Colors.HotPink);
            _cold = new SolidColorBrush(Colors.Cyan);
            _fire = new SolidColorBrush(Colors.Orange);
            _holy = new SolidColorBrush(Colors.AliceBlue);
            _lightning = new SolidColorBrush(Colors.Yellow);
            _physical = new SolidColorBrush(Colors.WhiteSmoke);
            _poison = new SolidColorBrush(Colors.GreenYellow);

            _wizardColors = new[] {_arcane, _cold, _fire, _lightning};
            _wdColors = new[] {_cold, _fire, _physical, _poison};
            _barbColors = new[] {_cold, _fire, _lightning, _physical};
            _crusaderColors = new[] {_fire, _holy, _lightning, _physical};
            _monkColors = new[] {_fire, _holy, _lightning, _physical};
            _dhColors = new[] {_cold, _fire, _lightning, _physical};

            _currentColors = _wdColors;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
            if (e.ChangedButton == MouseButton.Right)
                StartTimer();
            if (e.ChangedButton == MouseButton.Middle)
                Application.Current.Shutdown();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D1 || e.Key == Key.D2 || e.Key == Key.D3 || e.Key == Key.D4 || e.Key == Key.D5 || e.Key == Key.D6)
            {
                _elementsTimer.Stop();
                if (e.Key == Key.D1)
                {
                    _elements = new[] {"Cold", "Fire", "Lightning", "Physical"};
                    _class = " - Barb";
                    _currentColors = _barbColors;
                }
                if (e.Key == Key.D2)
                {
                    _elements = new[] {"Fire", "Holy", "Lightning", "Physical"};
                    _class = " - Crus";
                    _currentColors = _crusaderColors;
                }
                if (e.Key == Key.D3)
                {
                    _elements = new[] {"Cold", "Fire", "Lightning", "Physical"};
                    _class = " - DH";
                    _currentColors = _dhColors;
                }
                if (e.Key == Key.D4)
                {
                    _elements = new[] {"Fire", "Holy", "Lightning", "Physical"};
                    _class = " - Monk";
                    _currentColors = _monkColors;
                }
                if (e.Key == Key.D5)
                {
                    _elements = new[] {"Cold", "Fire", "Physical", "Poison"};
                    _class = " - WD";
                    _currentColors = _wdColors;
                }
                if (e.Key == Key.D6)
                {
                    _elements = new[] {"Arcane", "Cold", "Fire", "Lightning"};
                    _class = " - Wiz";
                    _currentColors = _wizardColors;
                }
                StartTimer();
            }
        }

        private void ElementsTimerTick(object sender, EventArgs e)
        {
            if (_elementSeconds < 3)
            {
                _elementSeconds += 1;

                if (_elementSeconds == 1)
                {
                    lblElement.Foreground = _currentColors[_elementSeconds];
                }
                else if (_elementSeconds == 2)
                {
                    lblElement.Foreground = _currentColors[_elementSeconds];
                }
                else if (_elementSeconds == 3)
                {
                    lblElement.Foreground = _currentColors[_elementSeconds];
                }
            }
            else
            {
                _elementSeconds = 0;
                lblElement.Foreground = _currentColors[_elementSeconds];
            }

            lblElement.Content = _elements[_elementSeconds];
        }

        private void StartTimer()
        {
            _elementSeconds = 0;
            _elementsTimer.Interval = new TimeSpan(0, 0, 4);
            _elementsTimer.Start();
            lblElement.Foreground = _currentColors[_elementSeconds];
            lblElement.Content = _elements[_elementSeconds];
            lblClass.Content = _class;
        }
    }
}