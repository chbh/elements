# README #

* Left click to drag the tracker around
* Right click to start the tracker
* Middle click to close the tracker

* 1-6 to select class and reset the timer.

* Ctrl + F1 resets the timer, even if the window is not in focus.

Order the elements come in based on the ring description:

* Arcane, Cold, Fire, Holy, Lightning, Physical, Poison.

Class order

* Barb:   Cold, fire, lightning, physical

* Crus:   Fire, holy, lightning, physical

* DH:     Cold, fire, lightning, physical

* Monk:   Fire, holy, lightning, physical

* WD:     Cold, fire, physical, poison

* Wiz:    Arcane, cold, fire, lightning